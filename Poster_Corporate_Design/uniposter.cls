\ProvidesClass{uniposter}[2017-11-18 v1 a0poster class DRRDietrich]
\LoadClassWithOptions{sciposter}

% Wichtige Pakete
\usepackage{algorithmic}
\usepackage[utf8]{inputenc} % Control input encoding
\usepackage[T1]{fontenc}      % Direkte Eingabe von Umlauten
\usepackage{url}              % Verbatim with URL-sensitive line breaks
\usepackage{textcomp}         % Textsymbole
\usepackage{varioref}         % Ermoeglicht \vref fuer bessere Referenzen
\usepackage[pdftex]{graphicx} % Grafiken
\usepackage{pict2e}           % Das muss unbedingt, sonst sind die Grafiken schrottig
\usepackage[german]{babel}    % Deutsche Formatierung und Rechtschreibung
\usepackage{german}           % Neue deutsche Rechtschreibung
\usepackage{amsmath}          % Mathe-Kram
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{indentfirst}      % Ersten Absatz einer Sektion einruecken
\usepackage{color}            % Text- und Hintergrundfarben
\usepackage[table, svgnames]{xcolor}
\usepackage{multicol}         % Ermoeglicht Wechsel zwischen ein- und zweispaltig
\usepackage{multirow}
\usepackage{verbatim}         % Verbesserte Verbatim-Umgebung
\usepackage{alltt}            % Alltt-Umgebung
\usepackage{hyperref}         % Verlinkung in den Verzeichnissen
\usepackage{bibgerm}
\usepackage{listings}
\usepackage{multirow}					%mehrere Zeilen / Splaten in Tabelle verbinden
\usepackage{tcolorbox}
\usepackage{background}
\usepackage{colortbl}
\usepackage{extarrows}
\usepackage{pbox}
\usetikzlibrary{arrows.meta}

\usepackage[top=1in, left=2in, right=2in]{geometry}

\setlength{\textwidth}{\textwidth-2in}

\usepackage{multicol} % This is so we can have multiple columns of text side-by-side
\columnsep=100pt % This is the amount of white space between the columns in the poster
\columnseprule=3pt % This is the thickness of the black line between the columns in the poster

\usepackage{times} % Use the times font
%\usepackage{palatino} % Uncomment to use the Palatino font

\usepackage{graphicx} % Required for including images
\graphicspath{{figures/}} % Location of the graphics files
\usepackage{booktabs} % Top and bottom rules for table
\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures
\usepackage{amsfonts, amsmath, amsthm, amssymb} % For math fonts, symbols and environments
\usepackage{wrapfig} % Allows wrapping text around tables and figures

\definecolor{Uni}{RGB}{1,74,153} % sets background colour of sections
\definecolor{evenrow}{RGB}{203,208,233} % sets background colour of sections
\definecolor{oddrow}{RGB}{231,233,239} % sets background colour of sections

\newcommand{\thead}[1]{\textcolor{white}{\textbf{#1}}}

\newcolumntype{C}[1]{>{\centering\arraybackslash}p{#1}} % zentriert mit Breitenangabe

\let\oldtabular\tabular
\let\endoldtabular\endtabular
\renewenvironment{tabular}[1]{\rowcolors{1}{oddrow}{evenrow}\oldtabular{#1}\rowcolor{Uni}}{\endoldtabular}

\renewcommand{\sPlainBoxSection}[1]{
  \vspace{\secskip}
  \begin{center}
    \setlength{\secboxwidth}{\columnwidth}
    \begin{tcolorbox}[width=\secboxwidth, height=1.8em, colback={Uni}, arc=3mm, boxrule=0pt, boxsep=.35em]
      \begin{center}
        \textcolor{white}{\textbf{#1}}
      \end{center}
    \end{tcolorbox}
  \end{center}
}

\newcommand\subtitle[1]{\renewcommand\@subtitle{#1}}
\newcommand\@subtitle{\@latex@error{No \noexpand\subtitle given}\@ehc}

\newcommand\institut[1]{\renewcommand\@institut{#1}}
\newcommand\@institut{\@latex@error{No \noexpand\institut given}\@ehc}

\newcommand\uni[1]{\renewcommand\@uni{#1}}
\newcommand\@uni{\@latex@error{No \noexpand\uni given}\@ehc}

\newcommand\logoleft[1]{\renewcommand\@logoleft{#1}}
\newcommand\@logoleft{\@latex@error{No \noexpand\logoleft given}\@ehc}

\newcommand\logoright[1]{\renewcommand\@logoright{#1}}
\newcommand\@logoright{\@latex@error{No \noexpand\logoright given}\@ehc}

\backgroundsetup{
	placement=bottom,
	angle=0,
	scale=1,
	opacity=1,
	hshift=-1in,
	vshift=-1in,
	contents={
		\Large
		\begin{tcolorbox}[width=841mm-5in, colback=Uni, arc=0.8in, boxsep=1.1cm, title=${}$, colbacktitle=white, sharp corners=south, colframe=Uni, toptitle=35.3in, boxrule=2mm]
			\textcolor{white}{
				\@author\\
				\@institut\\
				\@uni
			}
		\end{tcolorbox}
	}
}

\AtBeginDocument{
	\includegraphics[height=8.5cm]{\@logoleft}
	\hfill
	\includegraphics[height=8.5cm]{\@logoright}
	\vspace{6cm}
	\begin{center}
		\textcolor{Uni}{\veryHuge \@title}\\
		\vspace{1cm}
		\textcolor{Uni}{\huge \@subtitle}
	\end{center}
	\large
}